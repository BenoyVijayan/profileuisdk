//
//  ProfileViewController.swift
//  Login
//
//  Created by Binoy Vijayan on 19/11/19.
//  Copyright © 2019 Binoy Vijayan. All rights reserved.
//

import UIKit
import WebKit

public class ProfileViewController: UIViewController {

    private let webView = WKWebView()
    private let imagePicker = UIImagePickerController()
    private var didSubmit: ([String: Any]?) -> Void = { _ in }
    
    // bundle object for the sdk
    static var bunlde: Bundle {
        let tempBundle = Bundle(for: ProfileViewController.self)
        let bdl = Bundle(url: tempBundle.url(forResource: "ProfileUISdk", withExtension: "bundle")!)!
        return bdl
    }
    
    public static func instance(with callback: @escaping ([String: Any]?) -> Void ) -> ProfileViewController {
        let vc = ProfileViewController(nibName: "ProfileViewController", bundle: ProfileViewController.bunlde)
        vc.didSubmit = callback
        return vc
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        webView.frame = self.view.bounds
        self.view.addSubview(webView)
        guard let htmlPath = ProfileViewController.bunlde.path(forResource: "Login", ofType: "html") else {
            return
        }
        let url = URL(fileURLWithPath: htmlPath)
        webView.loadFileURL(url, allowingReadAccessTo: url)
        let request = URLRequest(url: url)
        webView.load(request)
        webView.configuration.userContentController.add(self, name: "clickImage")
        webView.configuration.userContentController.add(self, name: "clickSubmit")
        imagePicker.delegate = self
        
        /* // As the placeholder image is loaded by the html, this code is not required
        
         guard let imagePath = ProfileViewController.bunlde.path(forResource: "img_avatar2", ofType: "png") else {
             return
         }
         DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.loadAvatar(with: imagePath)
         })
        */
    }
}

extension ProfileViewController: WKScriptMessageHandler {
    public func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        let event = message.name
        
        switch event {
            case "clickImage":
                loadImagePicker()
            case "clickSubmit":
               
                let obj = (message.body as? [String: Any]) ?? [String: Any]()
                guard let profile = obj["profile"] as? [String: String]  else {
                        let prfl =  obj["profile"] as? [String: String]
                        didSubmit(prfl)
                        return
                }
                guard let imgPath = profile["image"]?.replacingOccurrences(of: "file://", with: ""), let img = UIImage(contentsOfFile: imgPath) else {
                    return
                }
                var nwDict = profile as [String: Any]
                nwDict["image"] = img
                didSubmit(nwDict)
                dismiss(animated: true, completion: nil)
            break
        default:
            print("Event not handled")
        }
    }
    
    func loadAvatar(with path: String) {
        webView.evaluateJavaScript("javascript:updateImage('\(path)');", completionHandler: { data, error in
            print(error.debugDescription)
        })
    }
}

extension ProfileViewController:  UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func loadImagePicker() {
        imagePicker.allowsEditing = false
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
        } else {
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }
    
   public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.imageURL] as? URL {
            self.loadAvatar(with: pickedImage.absoluteString)
        }
        dismiss(animated: true, completion: nil)
    }
}

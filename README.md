# ProfileUISdk

[![CI Status](https://img.shields.io/travis/Binoy Vijayan/ProfileUISdk.svg?style=flat)](https://travis-ci.org/Binoy Vijayan/ProfileUISdk)
[![Version](https://img.shields.io/cocoapods/v/ProfileUISdk.svg?style=flat)](https://cocoapods.org/pods/ProfileUISdk)
[![License](https://img.shields.io/cocoapods/l/ProfileUISdk.svg?style=flat)](https://cocoapods.org/pods/ProfileUISdk)
[![Platform](https://img.shields.io/cocoapods/p/ProfileUISdk.svg?style=flat)](https://cocoapods.org/pods/ProfileUISdk)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.


## Installation

ProfileUISdk is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line top of your Podfile:
```ruby
source 'https://BenoyVijayan@bitbucket.org/BenoyVijayan/bv-pod-specs.git'
```
Then add 

```ruby
# for latest version
pod 'ProfileUISdk'

# for specific version
pod 'ProfileUISdk', '0.4.0'
```

## How to use the SDK

-  import ProfileUISdk
- Create an instance of the ProfileViewController by calling 
```Swift

    /* callback is closure with an orptional param([String: Any]?) which has information about the profile such as name, email, phone and image */
    
    public static func instance(with callback: @escaping ([String: Any]?) -> Void ) -> ProfileViewController
```
##### Example
```Swift
     let profileVc = ProfileViewController.instance(with: {[weak self] profile in
    
               self?.update(info: profile)
     })
           
    present(profileVc, animated: true, completion: nil)
```

## Release new version of the SDK

*Make necessary chnages in the source code 

*run the script 'profile_ui_sdk_relase.sh' (cocoapod library deployment is autotmated with this script)
```shell
./profile_ui_sdk_relase.sh "commit message"
```
This will update the version of the sdk, so make sure to use the appropriate version in the pod file of your application. 

## Author

Binoy Vijayan, binoy.apple@gmail.com

## License

ProfileUISdk is available under the MIT license. See the LICENSE file for more info.

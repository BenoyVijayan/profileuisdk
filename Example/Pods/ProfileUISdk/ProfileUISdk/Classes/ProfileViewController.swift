//
//  ProfileViewController.swift
//  Login
//
//  Created by Binoy Vijayan on 18/11/19.
//  Copyright © 2019 Binoy Vijayan. All rights reserved.
//

import UIKit
import WebKit

class ProfileViewController: UIViewController {

    
    var webView: WKWebView!
    let imagePicker = UIImagePickerController()
    
    static var instance: ProfileViewController {
        
        return ProfileViewController(nibName: "ProfileViewController", bundle: Bundle(for: ProfileViewController.self))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView = WKWebView()
        webView.frame = self.view.bounds
        self.view.addSubview(webView)
        let frameworkBundle = Bundle(for: ProfileViewController.self)
        guard let url = frameworkBundle.resourceURL?.appendingPathComponent("ProfileUISdk.bundle/Login.html") else {
            return
        }
        webView.loadFileURL(url, allowingReadAccessTo: url)
        let request = URLRequest(url: url)
        webView.load(request)
        webView?.configuration.userContentController.add(self, name: "clickImage")
        webView?.configuration.userContentController.add(self, name: "clickSubmit")
        imagePicker.delegate = self
        guard let imagePath = Bundle.main.path(forResource: "test", ofType: "png") else {
            return
        }
        loadAvatar(with: imagePath)
    }

}

extension ProfileViewController: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        let event = message.name
        
        switch event {
            case "clickImage":
                loadImagePicker()
            case "clickSubmit":
               
                let obj = (message.body as? [String: Any]) ?? [String: Any]()
                
                guard let profile = obj["profile"] as? [String: String]  else {
                        let prfl =  obj["profile"] as? [String: String]
                        didSubmit(prfl)
                        return
                }
                
                
                
                guard let imgPath = profile["image"]?.replacingOccurrences(of: "file://", with: ""), let img = UIImage(contentsOfFile: imgPath) else {
                    return
                }
                var nwDict = profile as [String: Any]
                nwDict["image"] = img
                didSubmit(nwDict)
        
                dismiss(animated: true, completion: nil)
                
            break
        default:
            print("Event not handled")
        }
    }
    
    func loadAvatar(with path: String) {
        webView.evaluateJavaScript("javascript:updateImage('\(path)');", completionHandler: { data, error in
            print(error.debugDescription)
        })
    }
}

extension ProfileViewController:  UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func loadImagePicker() {
        imagePicker.allowsEditing = false
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
        } else {
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }
    
   public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.imageURL] as? URL {
            self.loadAvatar(with: pickedImage.absoluteString)
        }
        dismiss(animated: true, completion: nil)
    }
}

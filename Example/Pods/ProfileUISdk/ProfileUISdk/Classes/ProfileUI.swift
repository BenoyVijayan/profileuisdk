//
//  ProfileUI.swift
//  Login
//
//  Created by Binoy Vijayan on 18/11/19.
//  Copyright © 2019 Binoy Vijayan. All rights reserved.
//

import Foundation
import UIKit


public func show(in viewController: UIViewController, callback: @escaping ([String: Any]?) -> Void ) {
    let vc = ProfileViewController.instance
    viewController.present(vc, animated: true, completion: nil)
    didSubmit = callback
}

var didSubmit: ([String: Any]?) -> Void = { _ in }

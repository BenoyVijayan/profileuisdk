# ProfileUISdk

[![CI Status](https://img.shields.io/travis/Binoy Vijayan/ProfileUISdk.svg?style=flat)](https://travis-ci.org/Binoy Vijayan/ProfileUISdk)
[![Version](https://img.shields.io/cocoapods/v/ProfileUISdk.svg?style=flat)](https://cocoapods.org/pods/ProfileUISdk)
[![License](https://img.shields.io/cocoapods/l/ProfileUISdk.svg?style=flat)](https://cocoapods.org/pods/ProfileUISdk)
[![Platform](https://img.shields.io/cocoapods/p/ProfileUISdk.svg?style=flat)](https://cocoapods.org/pods/ProfileUISdk)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ProfileUISdk is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ProfileUISdk'
```

## Author

Binoy Vijayan, binoy.vijayan@go-jek.com

## License

ProfileUISdk is available under the MIT license. See the LICENSE file for more info.

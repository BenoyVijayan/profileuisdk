//
//  ViewController.swift
//  ProfileUISdk_Example
//
//  Created by Binoy Vijayan on 20/11/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import ProfileUISdk

class ViewController: UIViewController {
    
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var emailLabel: UILabel!
    @IBOutlet private weak var phoneLabel: UILabel!
    @IBOutlet private weak var avatarImage: UIImageView!
    var profileVc: ProfileViewController!

    @IBAction private func didTapShow(button: UIButton) {

        profileVc = ProfileViewController.instance(with: {[weak self] profile in
            self?.update(info: profile)
        })
        

        present(profileVc, animated: true, completion: nil)
    }
    
    private func update(info: [String: Any]?) {
        nameLabel.text = info?["name"] as? String ?? ""
        emailLabel.text = info?["email"] as? String ?? ""
        phoneLabel.text = info?["phone"] as? String ?? ""
        avatarImage.image = info?["image"] as? UIImage
    }
}
